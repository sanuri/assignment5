#include<stdio.h>

//maximum profit when increasing ticket price
void increprofit(int a,int p) {
	
	//price increases by 5
	
	int inc_p, inc_p_max=0, inc_t_p_max=0;
	
	/*
	inc_p = profit when increasing ticket price condition
	inc_p_max = maximum profit after increasing the ticket price
	inc_t_p_max = ticket price which he has the maximum profit after increasing 
	*/
	int n =0;
		while ((a - 20 * n) >= 0)
		{
			/*
			profit = income - expenses
			income = no. of attendees * ticket price 
			expenses = no. attendees * 3 + 500
			profit = ((a * p) * (p+500) - (3 * (a) + 500))
			*/
			
			inc_p = (( a - 20 * n) * (p + 5 * n) - (3 * (a - 20 * n) + 500));
			if(inc_p > inc_p_max)
	 	{
				
			inc_p_max = inc_p;
			inc_t_p_max = p + 5 * n; 
            
		}
			else 
		{	
		    //do nothing
	    }
	    
	    	n++;
		}
		printf("\nwhen increasing the ticket price: \n");
		printf("Ticket price = Rs.%d\nMaximum profit = Rs.%d\n\n", inc_t_p_max, inc_p_max);
	
	    
}

//maximum profit when decreasing the profit price

void decprofit(int a, int p){
	
	//price decreased by 5
	
	int dec_p, dec_p_max=0, dec_t_p_max=0;
	/*
	dec_p = profit when decreasing ticket price condition
	dec_p_max = maximum profit after decreasing the ticket price
	dec_t_p_max = ticket price which he has the maximum profit after decreasing
	*/
	int n = 0;
		while ((p - 5 * n) >= 0)
		{
			dec_p = ((a + 20 * n) * (p - 5 *n) - (3 * (a + 20 * n) + 500));
			if (dec_p > dec_p_max)
		{
			dec_p_max = dec_p;
			dec_t_p_max = p + 5 * n;
		}
			else
		{
			//do nothing
		}
			n++;
		}
		printf("\nWhen decreasing ticket price: \n");
		printf("Ticket price = Rs.%d\nMaximum profit = Rs.%d\n\n", dec_t_p_max, dec_p_max);	
		
}
int main(){
	int a , p;
	
	a = 120;// no. of attendees
	p = 15;//ticket price
	
	increprofit(a,p);
	decprofit(a,p);
return 0;

}

		



